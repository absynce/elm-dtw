module Main exposing (main)

import Css
import Css.Global
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Slides exposing (slidesDefaultOptions)
import Slides.Styles


main : Program () Slides.Model Slides.Msg
main =
    Slides.app
        { slidesDefaultOptions
            | style =
                [ Css.Global.img
                    [ Css.width Css.auto
                    ]
                ]
                    |> List.append
                        (Slides.Styles.elmMinimalist
                            (Css.rgb 255 255 255)
                            (Css.rgb 230 230 230)
                            (Css.px 30)
                            (Css.hex "323642")
                        )
            , title = "Make reliable web apps without JS fatigue"
        }
        [ titleSlide
        , myHistorySlide
        , pollAudience

        -- JS fatigue 1
        , whatIsJsFatigue

        -- , howJsFatigueGotMe
        , jsFatigueIsOverload

        -- Elm's response 1
        , whyElm
        , elmIsACohesiveEcosystem

        -- JS fatigue 2
        , jsFatigueIsUncertainty

        -- Elm's response 2
        , elmIsReliable
        , howElmIsReliableExample1CompilerError
        , howElmIsReliableExample2Maybe
        , howElmIsReliableExample2MaybeContinued

        -- , howElmIsReliableExample3CustomTypes
        -- JS fatigue 3
        , jsFatigueIsTheHeaviestObjectInTheUniverseImage
        , jsFatigueIsTheHeaviestObjectInTheUniverse

        -- Elm's response 3
        , elmIsASafePackageManager
        , howElmIsASafePackageEcosystemExample1NoSideEffects
        , howElmIsASafePackageEcosystemExample2SmallAssets

        -- JS fatigue 4
        , jsFatigueIsOverload2Dot0Dot0

        -- Elm's response 4
        , elmIsSimpleAndEasyToUse

        -- Pull it together
        , elmMakesHappyDevelopers

        -- Next steps
        , howToUseElm
        , embedInExistingApp -- ng or React

        -- , embedInExistingAppAngularReact -- ng or React
        , whatNext
        , thanks
        ]


titleSlide : Slides.Slide
titleSlide =
    Slides.html <|
        div [ class "title-slide" ]
            [ img
                [ alt "Elm Logo"
                , class "elm-logo"
                , src "images/elm-logo.svg"
                ]
                []
            , h1 [ class "sub-title" ] [ text "Make reliable web apps" ]
            , h2 [ class "sub-title" ] [ text "without JavaScript fatigue" ]
            , div []
                [ em [] [ text "by Jared M. Smith" ]
                ]
            ]


myHistorySlide : Slides.Slide
myHistorySlide =
    Slides.mdFragments
        [ "# My history"
        , "* Over 10 years of development experience"
        , "* Java -> C# -> JavaScript -> Elm"
        , """* JavaScript === My first 💖
  * Do (allthethings)
  * Quick feedback loop"""
        , "* Working alone vs. on a team" -- Mr. Solo Dolo vs. Responsibility, reliability.

        -- This is where I had Node/ngJS issues
        -- Physically and mentally tired.
        -- Introduced Elm.
        , "* Healthcare reliability"

        -- , "* Burn out \u{1F9EF}"
        , "* Elm in logistics/transportation" -- Started consulting, turned down full-time offers to write Elm.
        ]


{-|

    Questions:

    1. How many developers here have a background in web frontend development?
    2. Who has used TypeScript or Reason?
    3. Has anyone already used Elm?

-}
pollAudience : Slides.Slide
pollAudience =
    Slides.html <|
        div
            [ class "poll-audience"
            ]
            [ h1 [] [ text "Poll" ]
            , img
                [ alt "Show of hands"
                , src "images/poll.jpg"
                ]
                []
            , div []
                [ em []
                    [ text "image from "
                    , a
                        [ href "http://www.slate.com/articles/news_and_politics/politics/2016/05/how_to_read_a_poll.html" ]
                        [ text "slate.com" ]
                    ]
                ]
            ]


{-| We know one cause for JS fatigue: the rising number of frameworks, libraries, forks, and versions.
-}
whatIsJsFatigue : Slides.Slide
whatIsJsFatigue =
    Slides.html <|
        div
            [ class "what-is-js-fatigue"
            ]
            [ h2 [] [ text "What is JavaScript fatigue?" ]
            , div [ class "what-is-js-fatigue-image" ]
                [ img
                    [ alt "frontend roadmap 2018"
                    , src "images/frontend-roadmap-2018.png"
                    ]
                    []
                , div []
                    [ em [ class "source" ]
                        [ text "image from "
                        , a [ href "https://codeburst.io/the-2018-web-developer-roadmap-826b1b806e8d" ]
                            [ text "codeburst.io"
                            ]
                        ]
                    ]
                ]
            ]


{-| This creates decision fatigue.

My experience:
Each time I started a project I felt I needed to compare
frameworks and libraries to see if I should change my path.

"Due diligence" was masking a reliability concern, as well as a lack of cohesion.

TODO: Maybe switch title headers and image on left.

-}
jsFatigueIsOverload : Slides.Slide
jsFatigueIsOverload =
    Slides.htmlFragments
        [ h2 [ class "js-fatigue-is how-it-feels-title" ] [ text "JavaScript fatigue is..." ]
        , div [ class "how-it-feels-container" ]
            [ h1 [ class "js-fatigue-is-answer how-it-feels-title" ] [ text "overload" ]
            , div [ class "how-it-feels" ]
                [ img
                    [ alt "many framework logos"
                    , src "images/how-it-feels-to-learn-js-2016.png"
                    ]
                    []
                , div [ class "source-container" ]
                    [ em [ class "source" ]
                        [ text "image from "
                        , a
                            [ href "https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f"
                            , alt "\"How it feels to learn JavaScript in 2016\" by Jose Aguinaga"
                            ]
                            [ text "\"How it feels to learn JavaScript in 2016\"" ]
                        , text " by Jose Aguinaga"
                        ]
                    ]
                ]
            , ul [ class "how-it-feels-item" ] [ li [] [ text " New frameworks, libraries, versions, and forks." ] ]
            ]
        , ul [ class "how-it-feels-item" ] [ li [] [ text " How do they fit together?" ] ] -- Add difficult puzzle meme.
        ]


{-| My experience:
I found myself tacking on libraries such as lodash and immutability.js
to improve reliability. I would foster TDD but fought a losing battle.

TODO: Add examples of errors (AngularJS: done, mailspring, [rollbar top 10k JS errors](https://rollbar.com/blog/top-10-javascript-errors/)).

-}
jsFatigueIsUncertainty : Slides.Slide
jsFatigueIsUncertainty =
    Slides.htmlFragments
        [ h2 [ class "js-fatigue-is" ] [ text "JavaScript fatigue is..." ]
        , h1 [ class "js-fatigue-is-answer" ]
            [ text "uncertainty"
            , img
                [ alt "not sure if gusta meme"
                , class "not-sure-if-gusta"
                , src "images/not-sure-if-gusta.png"
                ]
                []
            ]

        -- , img
        --     [ alt "AngularJS undefined error"
        --     , class "angular-js-error"
        --     , src "images/js-undefined-error.png"
        --     ]
        --     []
        , ul [] [ li [] [ text "Guarding against null/undefined" ] ]
        , ul [] [ li [] [ text "How many tests are enough?" ] ]
        , ul [] [ li [] [ text "Fearing the refactor" ] ]
        ]


{-| -}
jsFatigueIsTheHeaviestObjectInTheUniverseImage : Slides.Slide
jsFatigueIsTheHeaviestObjectInTheUniverseImage =
    Slides.htmlFragments
        [ h4 [ class "heaviest-objects" ] [ text "JavaScript fatigue is..." ]
        , div []
            [ h3 [ class "heaviest-objects" ] [ text "the heaviest object in the universe" ]
            , div [ class "heaviest-objects" ]
                [ img
                    [ alt "heaviest object in the universe image (node_modules)"
                    , class "heaviest-objects"
                    , src "images/heaviest-objects-in-universe.png"
                    ]
                    []
                ]
            , div []
                [ em [ class "source" ]
                    [ text "image from "
                    , a
                        [ href "https://dev.to/leoat12/the-nodemodules-problem-29dc" ]
                        [ text "\"The node_modules problem\"" ]
                    , text " by Leonardo Teteo"
                    ]
                ]
            ]
        ]


{-| This leads to fearing package updates,
but doing so anyway to evade security issues.

My experience:
In a Node project, I had a dependency cause bugs w/o explicitly updating myself.
This was before package-lock, I didn't use shrinkwrap, and spent hours
with two (2) other devs searching for the failing dependency.

-}
jsFatigueIsTheHeaviestObjectInTheUniverse : Slides.Slide
jsFatigueIsTheHeaviestObjectInTheUniverse =
    Slides.htmlFragments
        [ div []
            [ h3 [ class "js-fatigue-is" ] [ text "JavaScript fatigue is..." ]
            , h2 [ class "js-fatigue-is-answer" ] [ text "the heaviest object in the universe" ]
            ]
        , ul [ class "heaviest" ] [ li [] [ text "The sheer number of dependencies" ] ] --
        , ul [ class "heaviest" ] [ li [] [ text "Reducing asset size" ] ] -- tree shaking, code splitting?
        , ul [ class "heaviest" ] [ li [] [ text "Updating to new versions" ] ] -- searching for NodeJS issue in Genesis.
        , ul [ class "heaviest" ] [ li [] [ text "Security issues" ] ] --
        ]


{-| Guess what? A new version of overload.js came out.

This creates decision fatigue in how we express things.

REMOVED: Show "expressiveness" of JS with Polacode snippets
from _If Hemingway Wrote JavaScript_.
TODO: Use 5 ways to toString and ES/Babel/TS Venn diagram. Break this into multiple slides.

-}
jsFatigueIsOverload2Dot0Dot0 : Slides.Slide
jsFatigueIsOverload2Dot0Dot0 =
    Slides.htmlFragments
        [ h2 [ class "js-fatigue-is" ] [ text "JavaScript fatigue is..." ]
        , h1 [ class "js-fatigue-is-answer" ] [ text "overload v2.0.0" ]
        , ul [] [ li [] [ text "ES5/6/7/8" ] ]
        , ul [] [ li [] [ text "Reason/React/JSX" ] ]
        , ul [] [ li [] [ text "TypeScript" ] ] -- TODO: Add Venn diagram.

        -- Maybe even mention CSS vs. elm-ui...
        ]


{-| Audience: Why Elm?
If you're saying frameworks are saturating my brain, why add another?
-}
whyElm : Slides.Slide
whyElm =
    Slides.html <|
        div
            [ class "why-elm"
            ]
            [ h1 [] [ text "Why Elm?" ]
            , img
                [ alt "PLZ not another JS framework"
                , src "images/not-another-js-framework.jpg"
                ]
                []
            ]


{-| Fade out other images and highlight Elm in <https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f>.
-}
elmIsACohesiveEcosystem : Slides.Slide
elmIsACohesiveEcosystem =
    Slides.htmlFragments
        [ div
            [ class "elm-reduces-js-fatigue"
            ]
            [ h3 [ class "how-elm-solves-overload" ] [ text "How Elm solves overload" ]
            ]
        , div
            [ class "elm-reduces-js-fatigue"
            ]
            [ h2 [ class "how-elm-solves-overload-with" ] [ text "with a cohesive ecosystem" ]
            ]
        , div [ class "how-elm-solves-overload-image-container" ]
            [ img
                [ alt "many framework logos blurred to show Elm focus"
                , class "how-it-feels-elm-focus"
                , src "images/how-it-feels-to-learn-js-2016-elm-focus-modal.png"
                ]
                []
            , div [ class "source-container" ]
                [ em [ class "source" ]
                    [ text "made from "
                    , a
                        [ href "https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f"
                        , alt "\"How it feels to learn JavaScript in 2016\" by Jose Aguinaga"
                        ]
                        [ text "\"How it feels to learn JavaScript in 2016\"" ]
                    , text " by Jose Aguinaga"
                    ]
                ]
            ]
        , elmIsA FirstElement "language" -- functional, simple, compiles to JS (for now)
        , elmIsA SubsequentElement "compiler" -- world reknown compiler error messages, no linter, babel config.
        , elmIsA SubsequentElement "single framework" -- TEA
        , elmIsA SubsequentElement "package manager" -- more on this later
        , elmIsA SubsequentElement "community" -- Solve XY problems, work together

        -- , img
        --     [ alt "JS fatigue vs. Elm"
        --     , src "images/js-fatigue-vs-elm.jpg"
        --     ]
        --     []
        -- , div []
        --     [ em []
        --         [ text "image from "
        --         , a
        --             [ href "https://github.com/dwyl/learn-elm/" ]
        --             [ text "github.com/dwyl/learn-elm" ]
        --         ]
        --     ]
        ]


type ElementPosition
    = FirstElement
    | SubsequentElement


elmIsA : ElementPosition -> String -> Html msg
elmIsA elementPosition whatElmIs =
    ul [ class "how-elm-solves-overload-item" ]
        [ li []
            [ span
                [ class "elm-is-a"
                , classList [ ( "first", elementPosition == FirstElement ) ]
                ]
                [ text "Elm is a " ]
            , span [ class "what-elm-is" ] [ text whatElmIs ]
            ]
        ]



-- {-|
-- -}
-- elmIsACohesiveEcosystem : Slides.Slide
-- elmIsACohesiveEcosystem =
--     Slides.htmlFragments <|
--         [ h2 [ class "how-elm-solves-overload" ] [ text "How Elm solves overload" ]
--         , h1 [ class "how-elm-solves-overload-with" ] [ text "with a cohesive ecosystem" ]
--         , ul [] [ li [] [ text "Elm is a language" ] ] -- functional, simple, compiles to JS (for now)
--         , ul [] [ li [] [ text "Elm is a compiler" ] ] -- world reknown compiler error messages, no linter, babel config.
--         , ul [] [ li [] [ text "Elm is a single framework" ] ] -- TEA
--         , ul [] [ li [] [ text "Elm is a package manager" ] ] -- more on this later
--         , ul [] [ li [] [ text "Elm is a community" ] ] -- Solve XY problems, work together
--         ]
--


elmIsReliable : Slides.Slide
elmIsReliable =
    Slides.htmlFragments
        [ h2 [ class "how-elm-solves-overload" ] [ text "How Elm solves uncertainty" ]
        , h1
            [ class "elm-is-reliable"
            , class "how-elm-solves-overload-with"
            ]
            [ text "Elm is reliable" ]
        , blockquote
            [ class "elm-is-reliable" ]
            [ text "\"No runtime exceptions in practice\"" ]
        , img
            [ alt "Elm runtime exception tweet"
            , class "runtime-exception-tweet"
            , src "images/elm-runtime-exception-tweet.gif"
            ]
            []

        --         , """<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">After 2 years and 200,000 lines of production <a href="https://twitter.com/elmlang?ref_src=twsrc%5Etfw">@elmlang</a> code, we got our first production runtime exception.
        --           <br/><br/>[...]
        --           </p>&mdash; Richard Feldman (@rtfeldman) <a href="https://twitter.com/rtfeldman/status/961051166783213570?ref_src=twsrc%5Etfw">February 7, 2018</a></blockquote>
        -- <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        -- """
        ]


{-| -}
howElmIsReliableExample1CompilerError : Slides.Slide
howElmIsReliableExample1CompilerError =
    Slides.html <|
        div
            [ class "elm-compiler-error"
            ]
            [ h1 [ class "how-elm-solves-overload-with" ] [ text "The Elm compiler has your back" ]
            , img
                [ alt "Elm compiler error - name misspelled"
                , src "images/elm-compiler-error.png"
                ]
                []
            ]


{-| TODO: Add a different maybe example matching theme.
-}
howElmIsReliableExample2Maybe : Slides.Slide
howElmIsReliableExample2Maybe =
    Slides.htmlFragments
        [ h1 [ class "how-elm-solves-overload-with" ] [ text "No null or undefined" ]
        , img
            [ alt "Maybe example"
            , class "maybe-example"
            , src "images/maybe-code.png"
            ]
            []
        ]


{-| -}
howElmIsReliableExample2MaybeContinued : Slides.Slide
howElmIsReliableExample2MaybeContinued =
    Slides.htmlFragments
        [ div [ class "maybe-example-case" ]
            [ pre [] [ text "Maybe something = Just something | Nothing" ]
            ]
        , img
            [ alt "Maybe example case"
            , class "maybe-example-case-image"
            , src "images/maybe-case.png"
            ]
            []
        ]


{-| -}
elmIsASafePackageManager : Slides.Slide
elmIsASafePackageManager =
    Slides.htmlFragments
        [ h3 [ class "how-elm-solves-overload" ] [ text "Elm solves the heaviest object in the universe" ]
        , h2 [ class "how-elm-solves-overload-with" ] [ text "with a safe package manager" ]

        -- elm diff elm-community/string-extra 2.0.0 4.0.0
        -- elm diff Microsoft/elm-json-tree-view 1.0.0 2.0.0
        , div []
            [ img
                [ alt "Elm diff output"
                , class "elm-diff-image"
                , src "images/elm-diff.png"
                ]
                []
            , div [ class "source-container" ]
                [ em [ class "source" ]
                    [ text "image from "
                    , a
                        [ href "https://elm-lang.org/"
                        , alt "Elm semantic diff on Microsoft/elm-json-tree-view"
                        ]
                        [ text "elm-lang.org" ]
                    ]
                ]
            ]
        ]


{-| Note: Integration with JS is through ports, not a direct interface.
-}
howElmIsASafePackageEcosystemExample1NoSideEffects : Slides.Slide
howElmIsASafePackageEcosystemExample1NoSideEffects =
    Slides.mdFragments
        [ "# Packages without side-effects"
        , "* Identify potential security concerns clearly"

        -- , "* Experiments with side-effects in a single channel: [elm-explorations](https://github.com/elm-explorations/)"
        , "* Future compile-to targets easier to implement" -- Because direct JS implementation is limited.
        , "* Enable asset optimization"
        ]


{-| No tree shaking required.

This is a segue to "How Elm solves overload v2.0.0".

TODO: Add meme of tree shaking game.

-}
howElmIsASafePackageEcosystemExample2SmallAssets : Slides.Slide
howElmIsASafePackageEcosystemExample2SmallAssets =
    Slides.html <|
        div
            [ class "small-assets"
            ]
            [ h1 [] [ text "Asset optimization" ]
            , div []
                [ text "with a single compiler flag ("
                , code [ class "optimize-flag" ]
                    [ text "--optimize" ]
                , text ")"
                ]
            , img
                [ class "small-assets-image"
                , src "images/asset-sizes.png"
                ]
                []
            , div []
                [ em [ class "source" ]
                    [ text "image from "
                    , a
                        [ href "https://elm-lang.org/blog/small-assets-without-the-headache"
                        , alt "Small Assets without the Headache"
                        ]
                        [ text "elm-lang.org" ]
                    ]
                ]
            ]


{-| -}
elmIsSimpleAndEasyToUse : Slides.Slide
elmIsSimpleAndEasyToUse =
    Slides.htmlFragments
        [ h2 [ class "how-elm-solves-overload" ] [ text "Elm solves overload v2.0.0" ]
        , h1 [ class "how-elm-solves-overload-with" ] [ text "with simplicity and ease of use" ]
        , ul [] [ li [] [ text "One way to do things" ] ]
        , ul [] [ li [] [ text "Few keywords" ] ] -- Having one way naturally leads to this.
        , ul [] [ li [] [ text "The language becomes simpler over time" ] ] -- APIs do not have to follow JS legacy, remove flip.
        , ul [] [ li [] [ text "Focused releases" ] ] -- Or "Do things right, not right now" - Evan
        ]


{-| -}
elmMakesHappyDevelopers : Slides.Slide
elmMakesHappyDevelopers =
    Slides.htmlFragments
        [ div [ class "happy-devs" ]
            [ img
                [ alt "happy little trees - Bob Ross"
                , class "happy-devs-image"
                , src "images/happy-devs-bob-ross.jpg"
                ]
                []
            , h1 [] [ text "Happy developers" ]
            ]

        -- , "# with all of the above"
        , ul [] [ li [] [ text "Cohesive ecosystem" ] ] -- For me, this was surprising. I thought I enjoyed searching and putting these together...
        , ul [] [ li [] [ text "Reliable" ] ] -- Reduced my overall anxiety when developing.
        , ul [] [ li [] [ text "Safe package manager" ] ] -- The semantic versioning was a big win, but security was an unexpected plus.
        , ul [] [ li [] [ text "Packages must have documentation" ] ]
        , ul [] [ li [] [ text "Simple and easy to use" ] ] -- Now when I switch back to Elm it's a breath of fresh air.
        , ul [] [ li [] [ text "Refactoring is enjoyable" ] ] -- I used to do everything to avoid compiler errors. Now I intentionally use them every day.
        ]


{-| -}
howToUseElm : Slides.Slide
howToUseElm =
    Slides.htmlFragments
        [ h2 [] [ text "✅ \"I'm convinced\"" ]
        , h1 [] [ text "\"How do I use Elm?\"" ]

        -- TODO: Add image?
        ]


{-| -}
embedInExistingApp : Slides.Slide
embedInExistingApp =
    Slides.htmlFragments
        [ h2
            [ class "embed-vanilla-js" ]
            [ text "Embed in an existing app" ]
        , h1
            [ class "embed-vanilla-js" ]
            [ text "with vanilla JS" ]
        , img
            [ alt "Elm runtime exception tweet"
            , class "embed-vanilla-js-image"
            , src "images/embed-vanilla-js.png"
            ]
            []
        , p [] [ text "...like this presentation!" ]
        ]


{-| Removed from slides.
-}
embedInExistingAppAngularReact : Slides.Slide
embedInExistingAppAngularReact =
    Slides.htmlFragments
        [ h2
            [ class "embed-angular" ]
            [ text "Embed in an existing app" ]
        , h1
            [ class "embed-angular" ]
            [ text "with AngularJS" ]
        , div []
            [ img
                [ alt "example Elm app embedded in AngularJS"
                , class "embed-angular-image"
                , src "images/embed-angular-js.png"
                ]
                []
            , div []
                [ em [ class "source" ]
                    [ text "using "
                    , a
                        [ href "https://github.com/absynce/angular-elm-components"
                        , alt "angular-elm-components"
                        ]
                        [ text "angular-elm-components" ]
                    ]
                ]
            ]
        ]


{-| -}
whatNext : Slides.Slide
whatNext =
    Slides.mdFragments
        [ "# What next?"
        , "* Go to Mike Onslow's workshop this afternoon!"
        , "* Check out the [official Elm guide](https://guide.elm-lang.org/)"
        , "* Try Elm on [Ellie](https://ellie-app.com/)" -- Like JSBin/Fiddle

        -- , "* ...or try my Intro to Elm series at [absynce.github.io](http://absynce.github.io)"
        , "* Embed in an existing app"
        ]


{-| Organizers, audience, family, employer.
-}
thanks : Slides.Slide
thanks =
    Slides.md
        """# Thanks!
Slides at [absynce.gitlab.com/elm-dtw](https://absynce.gitlab.com/elm-dtw)

Find me everywhere: @absynce

![My profile pic](images/jared-caricature.png)"""
